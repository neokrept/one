require('./code/server/lib/guardian/exceptions')

global._        = require('lodash')
global.Promise  = require('bluebird')
global.Big      = require('big.js')
global.Joi      = require('joi')
global.eJoi     = require('express-joi-validator')
global.utils    = require('./code/server/lib/utils')
global.db       = require('./code/server/lib/datastore').dynamodb
global.settings = require('./code/server/config/system.json')
global.bunyan   = require('bunyan')
global.request  = Promise.promisify(require("request"))

const cookieParser = require('cookie-parser'),
           express = require('express'),
              path = require('path'),
           favicon = require('serve-favicon'),
        bodyParser = require('body-parser'),
          myLogger = require('./code/server/lib/logger'),
            logger = bunyan.createLogger( { name: 'public' } ),
               app = express();

var bklog     = console.log
_.map(myLogger, (value, key) => console[key] = value)
console.$log  = bklog

// view engine setup
app.set('env', DEVMODE?'development':'production');
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(require('express-bunyan-logger').errorLogger());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

//app.use(express.static(path.join(__dirname, 'public')));

require('./code/server/routes/main')(app)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
