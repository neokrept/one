GLOBAL.eDatabaseError = function (message) {
  this.name     = "DatabaseError";
  this.message  = message;
  this.system   = "model";
  this.severity = "critical";
//  this.stack    = (new Error()).stack;
}
eDatabaseError.prototype = Object.create(Error.prototype);

GLOBAL.eDataIntegrityError = function(message) {
  this.name     = "DataIntegrityError";
  this.message  = message;
  this.system   = "live";//anything that might have to do with the user input
  this.severity = "high";
//  this.stack    = (new Error()).stack;
}
eDataIntegrityError.prototype = Object.create(Error.prototype);

GLOBAL.eNotFound = function(message) {
  this.name     = "NotFoundError";
  this.message  = message;
  this.system   = "model";
  this.severity = "low";
}
eNotFound.prototype = Object.create(Error.prototype);

GLOBAL.eValidation = function(message) {
  this.name     = "ValidationFailure";
  this.message  = message;
  this.system   = "model";
  this.severity = "low";
}
eValidation.prototype = Object.create(Error.prototype);
