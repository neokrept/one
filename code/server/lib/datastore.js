import vogels from 'vogels'
import prompt from 'prompt'

let askForReset = () => {
  return new Promise((resolve, reject) => {
    if (!DEVMODE) return resolve(false)//skip for production

    prompt.start()

    let onErr = (err) => {
      console.$log('Failure at prompt!', err)
      reject()
    }

    console.$log('Should we reset?')
    prompt.get(['reset'], (err, result) => {
      if (err) { return onErr(err) }
      if (result.reset.charAt(0).toLowerCase() === 'y') {
        console.$log('Autoresetting...')
        resolve(true)
      } else {
        console.$log('Autoresetting skipped!')
        resolve(false)
      }
    })
  })
}

let dynamodb = {
  ready: (models) => {
    return askForReset().then((autoReset) => {
      return Promise.each(models, (model) => new Promise((resolve, reject) => {
        if (model.settings.autoreset && DEVMODE && autoReset) {
          model.describeTable((err, data) => {//we can only delete if the table is present beforehand
            if (data) {
              console.log(`Autoresetting ${model.tableName()}`)
              model.deleteTable((err, data) => {
                if (!err)
                  return resolve()
                reject()
              })
            } else {
              resolve()//already reset... :)
            }
          })
        } else {
          resolve()
        }
      })).then(() => Promise.each(models, (model) => new Promise((resolve, reject) => {
          let tableName = model.tableName()

          model.describeTable((err, data) => {
            if (!data) {
              return new Promise((resolve, reject) => {
                model.createTable(model.getSettings(), (err, data) => {
                  if (err) {
                    throw new eDatabaseError("Cannot create table:", tableName)
                  } else {
                    console.log('Successfully created table:', tableName)
                    resolve(tableName)
                  }
                })
              }).then(() => {
                resolve(tableName)
              })
            } else {
              resolve(tableName)
            }
          })
        })
      ))
    })
  },
  defineModel: (name, schema, methods = {}, superUpdate) => {
    let model

    try {
      schema.schema =_.extend(schema.schema, {
        // used for concurrency support
        version:  Joi.number().required()
      })
      model = vogels.define(name, schema)
    } catch(err) {
      console.log(err)
      console.log('Schema conflict! You should most probably recheck schema or if non-vital data, you can even delete the tables(they auto-create if missing)')
      throw new eDatabaseError('Schema conflict!')
    }

    let _upd  = model.update,
        _crt  = model.create,
        _getM = model.getItems,//get Multiple of <Item>
        _get  = model.get

    /* The extension of all models */
    _.extend(methods, {//here we define the overwrites to the default model methods to account for versioning
      getSettings: () => {
        return {
          readCapacity  : (model.settings) ? ( (model.settings.readCapacity)  ? model.settings.readCapacity  : 1) : 1,
          writeCapacity : (model.settings) ? ( (model.settings.writeCapacity) ? model.settings.writeCapacity : 1) : 1
        }
      },
      create: (object) => {
        return new Promise((resolve, reject) => {
          return model.get(object[schema.hashKey]).then((data) => reject('Duplicate hash value!'), () => {
            return _crt(_.extend({}, object, { version : 0 }), (err, data) => {
              if (err)
                return reject(err)
              resolve(data)
            })
          })
        })
      },
      update: (object, options) => {
        //console.log('update: (object, options) -> (',object,options,')')
        return new Promise((resolve, reject) => {
          return _upd(object, options, (err, data) => {
            if (err)
              return reject(err)
            resolve(data)
          })
        })
      },
      updateSafe: (selector, patch, conditions = {}, limit = 301) => {
        //console.log(`updateSafe: (selector, patch, conditions) -> (`, selector, patch, conditions, `)`)
        return new Promise((resolve, reject) => {
          return model.get(selector, {ConsistentRead: true}).then((data) => {
            let version     = data.attrs.version

            patch = _.extend({}, patch, { version : version + 1 })

            let protection = {
              ConditionExpression:        '#_version < :version',
              ExpressionAttributeNames:   {
                '#_version': 'version'
              },
              ExpressionAttributeValues:  {
                ":version": version + 1
              }
            }

            _.each(conditions, (value, key) => {
              protection.ConditionExpression                  += ` AND #_${key} = :${key}`
              protection.ExpressionAttributeNames[`#_${key}`]  = key
              protection.ExpressionAttributeValues[`:${key}`]  = value
            })

            let object = _.extend({}, selector, patch)

            return model.update(object, protection).then((data) => {
              return resolve(data)
            }, () => {
              if (limit > 1) {
                console.$log(selector, patch, `With limit = ${limit}, retrying...`)
                return Promise.delay(utils.random.inBetween(86, 144)).then(() => model.updateSafe(selector, patch, conditions, limit - 1)).then((res) => {
                  console.$log(selector, patch, 'updateSafe retry success!')
                  resolve(res)
                })
              } else {
                console.$log('journaling...')
                return models.Journal.log(`updateSafe failed for ${JSON.stringify(selector)}, ${JSON.stringify(patch)}, ${JSON.stringify(conditions)}`).then(reject)
              }
            })
          }, reject)
        })
      },
      get: (hash) => {
        return new Promise((resolve, reject) => {
          _get(hash, (err, data) => {
            if (!err && (_.isObject(data) && data.attrs)) {
              return resolve(data)
            }
            reject(err || `Cannot find ${model.tableName()}['${hash}']!`)
          })
        })
      },
      getMulti: (hashes) => {
        hashes = (_.isArray(hashes)?hashes:[hashes])

        if (hashes.length === 1)
          return model.get(hashes[0])

        return new Promise((resolve, reject) => {
          _getM(hashes, (err, items) => {
            if (!err)
              return resolve(items)
            reject(err)
          })
        })
      },
      getAll: (filter = {}) => {
        //console.log('[WARNING!] Testing function(getAll), very poor performance, don`t use in production!')
        let query = model.parallelScan(settings.scanSegments)
        _.each(filter, (v, k) => {
          query.where(k).equals(v)
        })

        return new Promise((resolve, reject) => {
          query.exec((err, data) => {
            if (err)
              return reject(err)
            resolve(data)
          })
        })
      },
      removeAll: (filter) => {
        return model.getAll(filter).then((data) => {
          return Promise.map(data.Items, (item) => {
            item = item.attrs
            return model.remove(item[schema.hashKey])
          }, {concurrency: settings.concurrency})
        })
      },
      remove: (hash) => {
        return new Promise((resolve, reject) => {
          model.destroy(hash, (err) => {
            if (err)
              return reject(err)
            resolve()
          })
        })
      },
      getByIndex: (index, searchQuery) => {
        return new Promise((resolve, reject) => {
          try {
            //.descending(),.ascending()
            model.query(searchQuery).usingIndex(index).exec((err, data) => {
              if (data && data.Items && data.Items.length > 0) {
                data = data.Items.map((item) => item.attrs)
                return resolve(data)
              }
              reject(err)
            })
          } catch (err) {
            reject(err)
          }
        })
      },
      count: (filter) => {
        let query = model.parallelScan(settings.scanSegments)
        _.each(filter, (v, k) => {
          query.where(k).equals(v)
        })

        return new Promise((resolve, reject) => {
          query.select('COUNT').exec((err, data) => {
            if (err)
              return reject(err)
            resolve({count: data.Count, total: data.Total})
          })
        })
      },
      __911_empty:(hash = 'email') => {
        let query = model.parallelScan(settings.scanSegments)

        return new Promise((resolve, reject) => {
          query.exec((err, data) => {
            //console.log(data)
            return Promise.map(data.Items, (item) => {
              return model.remove(item.attrs[hash])
            }, {concurrency: settings.concurrency}).then((data) => {
              resolve(data)
            }).catch((err) => {
              console.log('Problems here at empty =>', err)
            //  throw new eDatabaseError(err)
            })
          })
        })
      }
    })
    _.extend(model, methods)
    /* The extension of all models */

    if (settings.debugging.runnables || settings.debugging.promises) {
      let logger = bunyan.createLogger({name: `${name}.model`})//one logger / module
      let beautifyArguments = (args) => {
        return _.map(args, (arg) => {
          let str = JSON.stringify(arg, null, 2),
             luck = utils.random.inBetween(300, 500)
          if (arg && arg.attrs)
            str = JSON.stringify({shorted: true, attrs: arg.attrs}, null, 2)
          if (arg && arg.Items)
            str = `(${Math.min(2, arg.Items.length)}/${arg.Items.length} items listed)` + JSON.stringify(_.map(_.range(Math.min(2, arg.Items.length)), (item, i) => {
              return {shorted: true, attrs: arg.Items[i].attrs}
            }), null, 2)
          return (!str)?arg:((str.length < 600)?str:`${str.substring(0, luck)}... (${luck}/${str.length})`)
        })
      }

      model = _.transform(model, function(object, value, key) {
        // console.log(`k[${name}.${key}]`)
        if (!!methods[key]) {
          switch (key) {
            default: {
              object[key] = function() {
                let result = value(...arguments)

                if (_.isFunction(result.then) && settings.debugging.promises) {
                  let args = beautifyArguments(arguments)

                  Promise.resolve(result).then(function() {
                    args = [`OK *models.${name}.${key}() with params:`].concat(args.concat(`------- returned -------`).concat(...beautifyArguments(arguments)))
                    logger.info(...args)
                  }, function() {
                    args = [`KO *models.${name}.${key}() with params:`].concat(args.concat(`------- returned -------`).concat(...beautifyArguments(arguments)))
                    logger.info(...args)
                  })
                } else if (!_.isFunction(result.then) && settings.debugging.runnables) {
                  let args = [`=> models.${name}.${key}() with params:`].concat(...beautifyArguments(arguments)).concat(`------- returned -------`).concat(beautifyArguments(result))
                  logger.info(...args)
                }
                return result
              }
            }
          }
        } else {
          object[key] = value
        }
        return true
      })

      //update outside of context too, in the model file for example
      superUpdate(model)
    }

    //this is an opinionated decision, but I think it's ok in regard to web dev
    GLOBAL.models = GLOBAL.models || {}
    GLOBAL.models[name] = model

    if (_.isFunction(model._autostart)) {
      GLOBAL.callOnReady.push((msg) => {
        console.info(`----------------------------------------------models.${name}------------------------------------------------`)
        return model._autostart().then(() => {
          console.info(`---------------------------------------------/models.${name}------------------------------------------------`)
        })
      })
    }

    return model
  }
};

module.exports = {
  dynamodb
};
