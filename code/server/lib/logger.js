let section = []

let logger = {
  anyToString: function() {
    let line = ""
    _.map(arguments, (argument) => {
      if (_.isObject(argument)) {
        try {
         line += JSON.stringify(argument, null, 2)
        } catch(e) {
         line += '{ ' + _.reduce(_.keys(argument), (o, key) => { o.push(`${key}: ${argument[key]}`); return o }, []).join(', ') + ' }'
        }
      } else if (_.isArray(argument)) {
        line += '[ ' + _.reduce(argument, (o, item) => {
          o.push(logger.anyToString(item))
        }, []).join(', ') + ' ]'
      } else {
        line += argument
      }
      line += " "
    })
    return line
  },
  log: function() {
    section.push(logger.anyToString(...arguments))
  },
  info: function() {
    section.push(logger.anyToString(...arguments))
  },
  reveal: (sectionName, reversed = false) => {
    section = (reversed)?section.reverse():section
    if (reversed) console.$log(`[--------------------/${sectionName}---------------------]`); else console.$log(`[---------------------${sectionName}---------------------]`)
    console.$log(section.join('\n'))
    if (reversed) console.$log(`[---------------------${sectionName}---------------------]`); else console.$log(`[--------------------/${sectionName}---------------------]`)
    section = []
  }
}

module.exports = logger
