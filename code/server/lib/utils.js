'use strict'
import crypto from 'crypto'
import moment from 'moment'

let utils = {
  self: {
    append: (extra) => {
      _.extend(utils, extra)
      GLOBAL.utils = utils
    }
  },
  // array -> object
  arrayToObject: (data) => {
    return _.reduce(data, (o, item) => { o[item.key] = item.value; return o }, {})
  },

  // object -> array
  objectToArray: (data) => {
    return _.reduce(_.keys(data), (o, item) => { o.push({key: item, value: data[item]}); return o }, [])
  },

  crypto: {
    md5: (content) => crypto.createHash('md5').update(content).digest('hex')
  },

  big: {
    convert: (mixed) => (mixed.plus && mixed.minus)?mixed:new Big(mixed),
    random: {
      between: (min, max) => {
        min = utils.big.convert(min)
        max = utils.big.convert(max)

        let diff = max.minus(min)
        let fuzz = new Big(1).times(Math.random()).times(Math.random())//a nice hopefully fully random, 32 decimal precision

        return min.plus(diff.times(fuzz).round())
      }
    }
  },
  date: {
    getDateAt: (n = 0, unit = 'days', format = 'YYYY-MM-DD') => {
      let date = (n === 0)?moment():moment().add(n, unit)

      return moment(date).format(format)
    },
    isLastDayOfMonth: (n) => {//if today + n days === last day of a month
      let yesterday = utils.date.getDateAt(n - 1, 'days', 'YYYY-MM-DD')
      return (moment(yesterday).endOf('month').format('YYYY-MM-DD') === utils.date.getDateAt(n, 'days', 'YYYY-MM-DD'))
    }
  },
  random: {
    inBetween: (min, max) => min + Math.round((Math.random() * Math.abs(max - min))) | 0,
    pick: (list, no) => (no > 0)?((no > 1)?utils.random.pick(list, no - 1).concat([ list[utils.random.inBetween(0, list.length - 1)] ]):list[utils.random.inBetween(0, list.length - 1)]):[],
    coinFlip: (a, b, r = 0.5) => {//more accurate coinFlipping than ever before :)
      let chance = Math.random()
      /*
      let power  = 0
      for (; !(r < 10 && r >= 1); ++power)
        r *= 10
      chance *= Math.pow(10, power)
      */
      return (chance <= r)?a:b
    }
  },
  validate: {
    integer: (obj, def = 0) => {
      obj = parseInt(obj)
      if (_.isNumber(obj) && obj !== NaN)
        return obj |0
      return utils.validate.integer(def, 0)
    },
    schema: (schema, object, resolve, reject) => {
      return Joi.validate(object, schema, (err, data) => {
        if (err)
          return reject(data)
        resolve(data)
      })
    },
    email: (email, resolve, reject) => {
      return utils.validate.schema(
        { email: Joi.string().email() },
        { email },
        resolve,
        reject
      )
    },
    leg: (email) => _.isString(email) && email.length > 1
  },
  trace: () => {
    let i, j, k, e = new Error('dummy')

    let stack = e.stack.replace(/^[^\(]+?[\n$]/gm, '')
      .replace(/^\s+at\s+/gm, '')
      .replace(/^Object.<anonymous>\s*\(/gm, '{anonymous}()@')
      .split('\n')
    return stack.join('\n')
  },
  json: {
    reverse: (map, callback) => {
      callback = callback || _.identity;

      return _.transform(map, function (memo, value, key) {
        key = callback(key);
        memo[value] || (memo[value] = []);
        memo[value].push(key);
      }, {});
    }
  },
  object: {
    applyToConstructor: (constructor, args) => {
      var args = [null].concat(args);
      var factoryFunction = constructor.bind.apply(constructor, args);
      return new factoryFunction();
    },
    craft: function(behaviour, state) {
      let obj = behaviour(state)
      if (_.isFunction(obj.$init))
        obj.$init()
      return obj
    },
    create: function() {
      var behaviours = _.map(arguments, (item) => item)
      let state

      if (behaviours.length > 1)
        state = behaviours.pop()
      else
        state = {}

      let classFactory = function(...behaviours) {
        let newClass = utils.object.assign.call(null, Object.create(null), behaviours, _.map(behaviours, (behaviour) => utils.object.craft(behaviour, state)))
        return newClass
      }
      let newObject   = utils.object.applyToConstructor(classFactory, behaviours)
      newObject.state = state

      return newObject
    },
    append: (obj, ...behaviours) => utils.object.assign.call(null, obj, behaviours, [obj].concat(_.map(behaviours, (behaviour) => utils.object.craft(behaviour, obj.state)))),
    assign: (self, functions, objects) => {
      let tags      = _.reduce(functions, (o, fn) => { o.push(fn.name); return o }, [])
      let ballOfFur = Object.assign.apply(Object.create(null), [self].concat(objects))
      ballOfFur.$identity = {
        behaviours: (ballOfFur.$identity)?ballOfFur.$identity.behaviours.concat(tags):tags
      }
      return ballOfFur
    },
    is: (subject, ...adjectives) => adjectives.every((v, i) => subject.$identity.behaviours.indexOf(v) !== -1)
  }
}

module.exports = utils
