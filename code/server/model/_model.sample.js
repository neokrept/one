/* THIS IS A SAMPLE STARTUP TEMPLATE, PLEASE MODIFY <MODELNAME>
AND IT'S SCHEMA. ALL THE METHODS IMPLEMENTED ARE EXPECTED TO RETURN A PROMISE.
COPY WITHOUT THIS LINES, BUT ONLY AFTER READING THIS PARAGRAPH */

import vogels from 'vogels'

let fields = {
  token     : Joi.string().regex(/^[a-zA-Z0-9]{8}$/).required(),
  recruiter : Joi.string().email().required()
}

let methods = {
  getCount: () => {
    return model.count({})
  },
  empty: () => {//don't implement in non-test models!
    return model.__911_empty('token')//hash key name
  }
}

let extraMethods = {  /* test, settings, maintenance & debugging methods */
  settings: {
    readCapacity  : 1,
    writeCapacity : 1
  },
  _autostart: () => {
    return Promise.resolve()
  }
}

let model = db.defineModel('Token', {
  hashKey    : 'token',

  // add the timestamp attributes (updatedAt, createdAt)
  timestamps : true,
  schema     : fields
}, _.extend({}, methods, extraMethods), (updatedModel) => {
  model = updatedModel
})

module.exports = model
