var express = require('express'),
    methods = require('./lib/methods')

module.exports = (app) => {
  app.get('/', function(req, res, next) {
    methods.time.getUpdate().then((data) => {
      res.json(data)
    })
  })
}
